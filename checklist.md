# Drupal 8 Migration Checklist

## Introduction

Lorem.

---

## Modules

### Custom Modules

For each custom module:

* Do you still need its functionality?

* Does D8 core offer suitable replacement? Evaluate the migration path.

* Is there a suitable D8 contrib module replacement? Evaluate the migration path.

* If no replacement, evaluate cost and effort for custom port to D8.

### Contributed Modules

For each contributed module:

* Do you still need its functionality?

* Does it have a D8 release? If not, is there an active issue with D8 port discussion?

* Does the D8 version have a migration path? Evaluate cost and effort for custom migration path.

* Is there a suitable D8 contrib module replacement? Evaluate the migration path.

* Evaluate cost/effort for custom port to D8.



---

## Configuration

### Fields

All fieldable entitities have the same issue:

* Are you using custom field types?
  * Yes? Evaluate cost/effort to port field definition code + upgrade path for field data.

* Are you using contrib field types? 
  * Yes? Is there a D8 port? Does it have an upgrade path? Do you need to port it yourself?

### Content types

For each CT:
* Do you still need this CT?

* Do you still need all the fields on this CT?

* Are you using custom / contrib field types? See "Fields" section.

* Are you using custom view mode settings? (@TODO: how these get migrated?)

* Are you using custom node / field templates?

### Vocabularies and Taxonomy terms

For each Vocabulary:

* Do you still need this vocabulary?

* Are you using custom / contrib field types? See "Fields" section.

* Are you using custom term / field templates?

### Users

* Are you using different profiles? (@TODO: how these get migrated?)

* Are you using custom / contrib field types? See "Fields" section.

* Password migration (@todo: how do these get migrated?)

### Image Styles


---

## Data

### Entity Reference issues

All entities have the same issue that needs to be carefully considered:

For each entity you **don't** want to import:

* Are other entities referencing this node?

* If other entities are referencing this node, what do you want to do?
  * Change your mind and import the node after all.
  * Go ahead and lose the reference.
  * Reference a different entity.
  * Something else?

 
### Content (nodes)

For each content type:

* Do you still need all of the content?
  * If no, what are the selection criteria? Which items do you want to migrate?

* Is all of the linked data still required (entity references, images, ...)?

* For each node you don't want to import, consider the entity reference issue mentioned earlier.

### Comments

For each comment type:

* Do you still need all the comments?
  * If no, what are the selection criteria? Which items do you want to migrate?

* For each comment you don't want to import, consider the entity reference issue mentioned earlier.


### Taxonomy Terms

For each vocabulary:

* Do you still need all the comments?
  * If no, what are the selection criteria? Which items do you want to migrate?

* For each taxonomy term you don't want to import, consider the entity reference issue mentioned earlier.


### Users

* Do you still need all the users?
  * If no, what are the selection criteria? Which items do you want to migrate?

* For each user you don't want to import, consider the entity reference issue mentioned earlier.


### Managed Files

* For each managed file you don't want to import, consider the entity reference issue mentioned earlier.


## Editorial Aspects

### Workflow

* is there an editorial workflow in place? Does it still make sense as-is?

* (@todo is there a migration path from D7 workbench_moderation to D8 workflows?)

* if your D7 workflow is not based on workbench_moderation, can it be reworked to fit the code D8 workflow? EStimate custom upgrade path.

### Content Access Control

* Are you using a non-core content access control mechanism?

* Is there a replacement solution with upgrade path?


### Media Handling

* how are files / images uploaded? Are they reused? Should they be reused?

* do you use the concept of a media library to reuse previously uploaded content?
  * yes: can it be reworked to fit the new D8.7 media library concept?

